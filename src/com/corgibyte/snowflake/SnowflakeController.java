package com.corgibyte.snowflake;

import java.awt.event.*;


/**
 * Controls the main program logic of the Koch snowflake program.
 *
 * @author Hannah Young
 * Created by hannah on 5/7/14.
 */
public class SnowflakeController {
    private KochDrawable koch; //current koch curve
    private SnowflakeView view;
    private boolean isSetToSnowflakeMode;

    /**
     * Initializes both the controller and the view.
     */
    public SnowflakeController() {
        koch = new Koch(new Point(0, 325), new Point(750, 325));
        view = new SnowflakeView(new EventListeners());
        isSetToSnowflakeMode = false;
        updateView();
    }

    /**
     * Start.
     *
     * @param args not used
     */
    public static void main(String[] args) {
        new SnowflakeController();
    }

    /**
     * Updates the view with the current state.
     */
    public void updateView() {
        if (koch != null) { //if koch isn't null:
            view.drawKoch(koch.getPoints()); //gives the points to be drawn
            view.setLabelToKochCurveLevel(koch.getLevel()); //and shows the current level
        } else {
            view.drawKoch(null); //else hands over null object
            view.setLabelToStart(); //and gives starting instructions
        }
    }

    /**
     * Bundles several different varieties of ActionListeners to be easily passed along.
     */
    public class EventListeners {
        public IncrementButtonListener incrementListener;
        public DecrementButtonListener decrementListener;
        public ModeSwitchButtonListener modeListener;
        public PanelMouseListener mouseListener;

        public EventListeners() {
            incrementListener = new IncrementButtonListener();
            decrementListener = new DecrementButtonListener();
            modeListener = new ModeSwitchButtonListener();
            mouseListener = new PanelMouseListener();
        }
    }

    /**
     * ActionListener for increment button.
     */
    private class IncrementButtonListener implements ActionListener {

        /**
         * When button is pushed, increments Koch curve if able.
         */
        @Override public void actionPerformed(ActionEvent actionEvent) {
            if (koch.getLevel() < 10) {
                koch.increment();
                updateView();
            } else {
                view.showWarning("Koch Curve cannot be greater than 10."); //won't increment over 10 due to performance issues
            }
        }
    }

    /**
     * ActionListener for decrement button.
     */
    private class DecrementButtonListener implements ActionListener {

        /**
         * When button is pushed, decrements Koch curve.
         */
        @Override public void actionPerformed(ActionEvent actionEvent) {
            if (koch.getLevel() > 0) {
                koch.decrement();
                updateView();
            } else {
                view.showWarning("Koch Curve cannot be lower than 0."); //can't go lower than 0 by definition
            }
        }
    }


    /**
     * ActionListener for mode switch button.
     */
    private class ModeSwitchButtonListener implements ActionListener {

        /**
         * Switches between snowflake and curve mode.
         * @param actionEvent
         */
        @Override public void actionPerformed(ActionEvent actionEvent) {
            koch = null;
            isSetToSnowflakeMode = !isSetToSnowflakeMode;
            updateView();
        }
    }


    /**
     * Listens for Mouse clicks on the drawing panel to set a new Koch curve.
     */
    private class PanelMouseListener extends MouseAdapter {
        private boolean lookingForSecondPoint;
        private Point firstPoint;

        private PanelMouseListener() {
            lookingForSecondPoint = false;
            firstPoint = null;
        }

        /**
         * For mouse click on drawing panel. If curve is already drawn, deletes it and sets a starting point
         * for a new curve. When clicked again, finishes the new Koch curve, drawing it on the screen.
         */
        @Override public void mouseClicked(MouseEvent event) {
            if (!lookingForSecondPoint) { //when mouse is clicked, check to see if we're already waiting for a second point, if we're not:
                firstPoint = new Point(event.getX(), event.getY()); //set a new point to be the start
                lookingForSecondPoint = true; //flags that we're waiting for another point
                koch = null; //delete current curve
                updateView(); //redraw blank...
                view.setLabelToOnClick(); //tell label to display instructions for second click
            } else { //if we're already waiting for that second point...
                Point secondPoint = new Point(event.getX(), event.getY()); //set the new point as second point
                if (isSetToSnowflakeMode) { //create new snowflake if in snowflake mode
                    koch = new KochSnowflake(firstPoint, secondPoint);
                } else {
                    koch = new Koch(firstPoint, secondPoint); //create new curve if not
                }
                lookingForSecondPoint = false; //remove flags
                firstPoint = null;
                updateView(); //redraw new curve
            }
        }
    }
}

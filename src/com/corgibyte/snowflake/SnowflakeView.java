package com.corgibyte.snowflake;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * Draws GUI and output for the Koch curve.
 *
 * @author Hannah Young
 * Created by hannah on 5/5/14.
 */
public class SnowflakeView {
    private KochDrawingPanel kochDrawPanel; //drawing panel for the koch curve
    private JLabel levelTextLabel; //instruction label
    private static final String START_TEXT = "Click to set the first point of the Koch Curve"; //text for instructions when no curve
    private static final String CLICK_TEXT = "Click a second point to finish drawing the Koch Curve"; //text for when starting point has been selected and second curve required

    /**
     * Instantiates new view.
     *
     * @param listeners bundle of listeners to attach to pieces of the view
     */
    public SnowflakeView(SnowflakeController.EventListeners listeners) {
        //set up frame
        JFrame frame = new JFrame("Koch Snowflake");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        //set up drawing panel
        kochDrawPanel = new KochDrawingPanel(listeners.mouseListener);

        //set up buttons
        JButton incrementButton = new JButton("Increment");
        incrementButton.addActionListener(listeners.incrementListener);
        JButton decrementButton = new JButton("Decrement");
        decrementButton.addActionListener(listeners.decrementListener);
        Container buttonPanel = new JPanel();
        JButton modeSwitchButton = new JButton("Switch Mode");
        modeSwitchButton.addActionListener(listeners.modeListener);
        buttonPanel.add(incrementButton);
        buttonPanel.add(decrementButton);
        buttonPanel.add(modeSwitchButton);

        //initialize the text label
        Container labelContainer = new JPanel();
        levelTextLabel = new JLabel();
        labelContainer.add(levelTextLabel);
        setLabelToStart();

        //add everything to the frame
        frame.getContentPane().add(BorderLayout.CENTER, kochDrawPanel);
        frame.getContentPane().add(BorderLayout.SOUTH, buttonPanel);
        frame.getContentPane().add(BorderLayout.NORTH, labelContainer);

        //initialize the frame
        frame.setSize(750, 750);
        frame.setVisible(true);
    }

    /**
     * Sets new points to be drawn by the KochDrawingPanel and then instructions the panel and refreshes it.
     * @param points points to be drawn
     * @see KochDrawingPanel#setPoints(java.util.List)
     */
    public void drawKoch(List<Point> points) {
        kochDrawPanel.setPoints(points);
        kochDrawPanel.repaint();
    }

    /**
     * Sets label with instructions when there is no Koch curve set yet.
     */
    public void setLabelToStart() {
        levelTextLabel.setText(START_TEXT);
    }

    /**
     * Sets label with instructions when one point has been selected to start a new Koch curve, but
     * the second has not been selected yet.
     */
    public void setLabelToOnClick() {
        levelTextLabel.setText(CLICK_TEXT);
    }

    /**
     * Sets label to display that the Koch curve is at the specified level.
     *
     * @param kochLevel level of the Koch curve being displayed
     */
    public void setLabelToKochCurveLevel(int kochLevel) {
        levelTextLabel.setText("Koch Curve Level: " + kochLevel);
    }

    /**
     * Shows generic warning box with specified text.
     *
     * @param warning text to be displayed in a warning
     */
    public void showWarning(String warning) {
        JOptionPane.showMessageDialog(kochDrawPanel, warning);
    }

}

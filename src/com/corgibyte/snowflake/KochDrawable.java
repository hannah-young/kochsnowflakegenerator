package com.corgibyte.snowflake;

import java.util.List;

/**
 * Interface for classes that are able to be drawn by the KochController.
 *
 * @author Hannah Young
 * Created by hannah on 5/8/14.
 */
public interface KochDrawable {
    /**
     * Provide list of points in sequential order to be drawn.
     *
     * @return list of points to be drawn
     */
    public List<Point> getPoints();

    /**
     * Returns current level of the Koch curves in the drawable.
     *
     * @return level of the curve
     */
    public int getLevel();

    /**
     * Increment the curves in the drawable up on level.
     */
    public void increment();

    /**
     * Decrement the curves in the drawable down one level.
     *
     * @throws java.lang.IllegalArgumentException if curves are already set to level 0
     */
    public void decrement();
}

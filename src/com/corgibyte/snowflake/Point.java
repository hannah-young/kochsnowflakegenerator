package com.corgibyte.snowflake;

/**
 * Immutable Point defined by an x and y coordinate.
 */
public class Point {
    private final int x, y;

    /**
     * Creates point at specified coordinates.
     * 
     * @param x x-coordinate
     * @param y y-coordinate
     */
    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    /**
     * Gets x-coordinate of point.
     * 
     * @return x-coordinate
     */
    public int getX() {
        return x;
    }
    
    /**
     * Gets y-coordinate of point.
     *
     * @return y-coordinate
     */
    public int getY() {
        return y;
    }

    @Override
    public boolean equals(Object o) {
        if (o.getClass() != Point.class) {
            return false;
        }
        Point p = (Point) o;
        return p.x == x && p.y == y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }
}

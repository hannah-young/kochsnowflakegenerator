package com.corgibyte.snowflake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.util.List;


/**
 * Panel that is used to draw a Koch curve or snowflake by being given a list of points.
 *
 * @author Hannah Young
 * Created by hannah on 5/5/14.
 */
public class KochDrawingPanel extends JPanel {
    List<Point> points; //points to draw
    Color backgroundColor;
    Color curveColor;

    private static final Color DEFAULT_BACKGROUND_COLOR = Color.BLACK;
    private static final Color DEFAULT_CURVE_COLOR = Color.WHITE;

    /**
     * Initializes the drawing panel.
     *
     * @param mouse MouseListener to be attached to this KochDrawingPanel
     */
    public KochDrawingPanel(MouseListener mouse) {
        this(mouse, DEFAULT_BACKGROUND_COLOR, DEFAULT_CURVE_COLOR);
    }

    public KochDrawingPanel(MouseListener mouse, Color backgroundColor, Color curveColor) {
        addMouseListener(mouse);
        this.backgroundColor = backgroundColor;
        this.curveColor = curveColor;
        setBackground(backgroundColor);
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g); //paints as required
        if (points != null) { //if there are specified points, draw them
            int[] xPoints = new int[points.size()]; //add to arrays
            int[] yPoints = new int[points.size()];
            for (int i = 0; i < points.size(); i++) {
                Point p = points.get(i);
                xPoints[i] = p.getX();
                yPoints[i] = p.getY();
            }
            g.setColor(curveColor);
            g.drawPolyline(xPoints, yPoints, xPoints.length); //feed them to the Graphics
        } else {
            g.setColor(backgroundColor); //if no points specified, make the canvas blank
            g.fillRect(0, 0, 10000, 10000);
        }

    }

    /**
     * Set the current Points to be drawn by this Drawing Panel.
     *
     * @param points points to be drawn
     */
    public void setPoints(List<Point> points) {
        this.points = points;
    }

}

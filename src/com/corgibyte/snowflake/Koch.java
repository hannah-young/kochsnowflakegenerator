package com.corgibyte.snowflake;

import com.google.common.collect.ImmutableList;
import java.util.*;

/**
 * Represents a single Koch curve (one third of a <a href="http://en.wikipedia.org/wiki/Koch_snowflake">Koch snowflake)</a>.
 *
 * @author Hannah Young
 * @version 5/6/2014
 */
public class Koch implements Iterable<Point>, KochDrawable {
    private static final double SIN_PI_3 = 0.86603; //used for calculating the peak, equal to sin(pi/3)
    private static final double COS_PI_3 = 0.5; //used for calculating the peak, equal to cos(pi/3)

    private int level; //level of the Koch curve
    private Point start, end; //points to comprise the beginning and end of the curve
    private List<Point> currentLevel; // list to hold list of points at the currently set level
    private Deque<List<Point>> lowerLevels; // stack to hold points below the current level
    private Deque<List<Point>> higherLevels; // stack to hold points above the current level

    /**
     * Create a new Koch curve with the specified starting and ending points. Initialized at level 0.
     * @param start starting point
     * @param end ending point
     */
    public Koch(Point start, Point end) {
        this.start = start;
        this.end = end;
        ImmutableList.Builder<Point> levelBuilder = new ImmutableList.Builder<Point>();
        levelBuilder.add(start);
        levelBuilder.add(end);
        currentLevel = levelBuilder.build();
        level = 0;
        lowerLevels = new ArrayDeque<List<Point>>();
        higherLevels = new ArrayDeque<List<Point>>();
    }

    /**
     * Gets the currently set level of the Koch curve.
     *
     * @return the currently level of the Koch curve
     */
    public int getLevel() {
        return level;
    }

    /**
     * Gets immutable list of points in the Koch curve at the current level. The Koch curve is defined
     * as being a series of line segments between each consecutive point, starting from the first.
     *
     * @return points in the Koch curve at the current level
     */
    public List<Point> getPoints() {
        return currentLevel;
    }

    /**
     * Increments the Koch curve up one level.
     */
    public void increment() {
        level++;
        lowerLevels.addFirst(currentLevel);
        if (higherLevels.peekFirst() != null) { //if the points have already been calculated, pull them from the top of the stack
            currentLevel = higherLevels.removeFirst();
        } else {
            currentLevel = generateLevel();
        }
    }

    /**
     * Decrements the Koch curve down one level.
     *
     * @throws java.lang.IllegalArgumentException if curve is already set to level 0
     */
    public void decrement() {
        if (level < 1) {
            throw new IllegalArgumentException("Cannot decrement Koch Curve below 0.");
        }
        level--;
        higherLevels.addFirst(currentLevel); //add the current points to the top of the stack of points of levels higher than currently set
        currentLevel = lowerLevels.removeFirst();
    }

    /**
     * Returns <a href="http://docs.oracle.com/javase/7/docs/api/java/util/Iterator.html">Iterator</a>
     * of points for this Koch curve at the current level. @link #getPoints() to see how points are defined.
     *
     * @return Iterator over points in the curve
     */
    public Iterator<Point> iterator() {
        return currentLevel.iterator();
    }

    /**
     * Generates points for the Koch curve to the currently set level.
     *
     * @return List of Points for the Koch curve at this level
     */
    private List<Point> generateLevel() {
        Queue<Point> points = new ArrayDeque<Point>(); //create queue to hold points
        points.add(start); //adding starting point
        for (Point p : generateLevel(level, start, end)) { //call to recursive function to find all middle points
            points.add(p);
        }
        points.add(end); //add ending point
        ImmutableList.Builder<Point> listBuilder = new ImmutableList.Builder<Point>(); //create immutable list from queue
        for (Point p : points) {
            listBuilder.add(p);
        }
        return listBuilder.build();
    }

    /**
     * Recursive function to generate points in a Koch curve. Each call to the function is responsible
     * finding all of the points BETWEEN the starting and ending point. Thus, when this function is
     * called to generate points, the calling function must append and prepend the start and end points
     * to the generated points if they are needed.
     *
     * The function's algorithm:
     * <ol>
     *     <li>Find the points A and B that are 1/3 of the segment's length from the beginning and end</li>
     *     <li>Find the "peak" point between and above A and B that forms an equilateral triangle with
     *     A and B</li>
     *     <li>If a deeper level of recursion is needed, it will recurse on the four segments that were
     *     formed: from the beginning to A, from A to the peak, from peak to B, and from B to the end.</li>
     *     <li>All of the points generated between the Start and the End will be returned.</li>
     * </ol>
     *
     * @param level the current level of recursion
     * @param start starting point
     * @param end ending point
     * @return Queue of Points generated between the two starting points up to the required level
     */
    private Queue<Point> generateLevel(int level, Point start, Point end) {
        int deltaX = end.getX() - start.getX();
        int deltaY = end.getY() - start.getY();
        Point oneThird = new Point(start.getX() + deltaX/3, start.getY() + deltaY/3); //find points 1/3 from beginning and end
        Point twoThird = new Point(end.getX() - deltaX/3, end.getY() - deltaY/3);
        Point peak = getPeak(oneThird, twoThird); //find peak
        Queue<Point> answer = new ArrayDeque<Point>(); //queue that will be returned
        if (level == 1) { //recursion base case: all the queue needs are the three inner points
            answer.add(oneThird);
            answer.add(peak);
            answer.add(twoThird);
        } else { //recursive case:  requires three inner points and all of the points between those points
            Queue<Point> startToOneThird = generateLevel(level - 1, start, oneThird); //each line segment is recursed upon
            Queue<Point> oneThirdToPeak = generateLevel(level - 1, oneThird, peak);
            Queue<Point> peakToTwoThird = generateLevel(level - 1, peak, twoThird);
            Queue<Point> twoThirdToEnd = generateLevel(level - 1, twoThird, end);
            for (Point p : startToOneThird) { //then add them all to the queue one piece at a time in proper order
                answer.add(p);
            }
            answer.add(oneThird);
            for (Point p : oneThirdToPeak) {
                answer.add(p);
            }
            answer.add(peak);
            for (Point p : peakToTwoThird) {
                answer.add(p);
            }
            answer.add(twoThird);
            for (Point p : twoThirdToEnd) {
                answer.add(p);
            }
        }
        return answer; //ta da!
    }

    /**
     * Finds peak between two points. The peak and the two given points form an equilateral triangle.
     *
     * @param a first point to form peak with
     * @param b second point to form peak with
     * @return peak point between a and b
     */
    static Point getPeak(Point a, Point b) {
        int deltaX = b.getX() - a.getX();
        int deltaY = a.getY() - b.getY();
        double peakX = a.getX() + deltaX * COS_PI_3 - deltaY * SIN_PI_3;
        double peakY = a.getY() - deltaX * SIN_PI_3 - deltaY * COS_PI_3;
        return new Point((int) peakX, (int) peakY);
    }
}

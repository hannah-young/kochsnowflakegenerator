package com.corgibyte.snowflake;

import com.google.common.collect.ImmutableList;

import java.util.Iterator;
import java.util.List;

/**
 * Creates a full Koch snowflake using three Koch curves.
 *
 * @author Hannah Young
 * Created by hannah on 5/8/14.
 */
public class KochSnowflake implements KochDrawable, Iterable<Point> {
    private Koch kochA;
    private Koch kochB;
    private Koch kochC;
    private List<Point> currentPoints;

    /**
     * Creates Koch snowflake from two specified points, A and B, and a third, calculated point, C.
     * C is calculated by finding an equilateral triangle between all three points, with C being above
     * A and B when A is to the left of B.
     *
     * @param a first point to form Koch snowflake
     * @param b second point to form Koch snowflake
     */
    public KochSnowflake(Point a, Point b) {
        Point c = Koch.getPeak(a, b);
        kochA = new Koch(a, c);
        kochB = new Koch(c, b);
        kochC = new Koch(b, a);
        updatePoints();
    }

    /**
     * Gets list of points that are currently part of the snowflake.
     *
     * @return immutable list of points in the snowflake
     */
    public List<Point> getPoints() {
        return currentPoints;
    }

    /**
     * Gets level of the Koch curves making up the snowflake.
     *
     * @return level of the curves in the snowflake
     */
    @Override public int getLevel() {
        return kochA.getLevel();
    }

    /**
     * Increments the curves in the snowflake up one level.
     */
    @Override public void increment() {
        kochA.increment();
        kochB.increment();
        kochC.increment();
        updatePoints();
    }

    /**
     * Decrements the curves in the snowflake down one level.
     *
     * @throws java.lang.IllegalArgumentException if curves are already at 0.
     */
    @Override public void decrement() {
        kochA.decrement();
        kochB.decrement();
        kochC.decrement();
        updatePoints();
    }

    /**
     * Provides Iterator to go over points in the snowflake.
     *
     * @return points in the snowflake
     */
    @Override public Iterator<Point> iterator() {
        return currentPoints.iterator();
    }

    /**
     * Updates the current points of the snowflake
     */
    private void updatePoints() {
        ImmutableList.Builder<Point> listBuilder = new ImmutableList.Builder<Point>();
        for (Point p : kochA) {
            listBuilder.add(p);
        }
        for (Point p : kochB) {
            listBuilder.add(p);
        }
        for (Point p : kochC) {
            listBuilder.add(p);
        }
        currentPoints = listBuilder.build();
    }
}
